<?php
/**
 * Class DM_FeaturedWidget_Block_Featuredwidget
 *
 * Rewrite of mess in DM_FeaturedWidget_Block_Featuredwidget
 *
 * @package     DM_FeaturedWidget
 * @author      Dmytro Shkilniuk <dm.magento@gmail.com>
 * @copyright   Dmytro Shkilniuk http://mysite.com
 * @version     0.1.0
 */

class DM_FeaturedWidget_Block_Featuredwidget
    extends Mage_Core_Block_Template
    implements Mage_Widget_Block_Interface
{

    private $productCount;
    private $showImages;
    private $showPrices;
    const USE_IN_FEATURED_WIDGET = 1;
    const NOT_USE_IN_FEATURED_WIDGET = 0;

    /**
     * @return string
     */
    protected function _toHtml()
    {
        $count = $this->getData('count');
        if($count){
            $this->setProductCount($count);
        }else{
            $this->setProductCount(5);
        }

        $showImages = $this->getData('showimages');
        if($showImages == "1"){
            $this->setShowImages(true);
        }else{
            $this->setShowImages(false);
        }

        $showPrices = $this->getData('showprices');
        if($showPrices == "1"){
            $this->setShowPrices(true);
        }else{
            $this->setShowPrices(false);
        }
        $this->setTemplate('dm/featured/widgetlist.phtml');
        return parent::_toHtml();
    }

    /**
     * @param $count
     */
    public function setProductCount($count)
    {
        $this->productCount = $count;
    }

    /**
     * @return int
     */
    public function getProductCount()
    {
        return $this->productCount;
    }

    /**
     * @param bool $show
     */
    public function setShowImages($show)
    {
        $this->showImages = $show;
    }

    /**
     * @return bool
     */
    public function getShowImages()
    {
        return $this->showImages;
    }

    /**
     * @param bool $show
     */
    public function setShowPrices($show)
    {
        $this->showPrices = $show;
    }

    /**
     * @return int
     */
    public function getShowPrices()
    {
        return $this->showPrices;
    }

    /**
     * @return object
     */
    public function getProducts()
    {
        $products = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH)
            ->addAttributeToFilter('use_in_featured_widget', self::USE_IN_FEATURED_WIDGET)
            ->addAttributeToFilter(
                'status',
                array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
            )
            ->setPageSize($this->getProductCount())
            ->setCurPage(1);
        Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($products);

        return $products;
    }

    /**
     * @param $_product
     * @return string
     */
    public function getPriceHtml($_product){
        $productBlock = $this->getLayout()->createBlock('catalog/product_price');
        return $productBlock->getPriceHtml($_product);
    }


    /**
     * @param $product
     * @return string
     */
    public function getAddToCartProductUrl($product){
        return Mage_Catalog_Block_Product_View::getAddToCartUrl($product);
    }
}