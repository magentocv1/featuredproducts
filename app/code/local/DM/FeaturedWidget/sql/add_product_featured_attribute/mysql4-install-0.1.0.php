<?php
$installer = $this;
$installer->startSetup();
$config = array(
    'group' => 'General',
    'attribute_set' => 'Default',
    'backend' => '',
    'frontend' => '',
    'label' => 'Use in featured widget',
    'type' => 'int',
    'input' => 'boolean',
    'source' => 'eav/entity_attribute_source_boolean',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible' => true,
    'required' => false,
    'user_defined' => true,
    'default' => '0',
    'searchable' => false,
    'filterable' => false,
    'comparable' => false,
    'visible_on_front' => false,
    'visible_in_advanced_search' => false,
    'used_in_product_listing' => true,
    'unique' => false,
    'apply_to' => '',  // Apply to All product type
    'class' => ''
);

$installer->addAttribute('catalog_product', 'use_in_featured_widget', $config);
